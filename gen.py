#!/usr/bin/env python
import requests
import json

url = "https://api.burgerking.de/api/o2uvrPdUY57J5WwYs6NtzZ2Knk7TnAUY/v4/de/en/coupons/"

r = requests.get(url)

if r.status_code != 200:
    print("Not 200")
    exit(0)

j = json.loads(r.text)

print("Found {} coupons".format(len(j)))

# iterate over coupons
for c in j:

    # Dump coupon to detail-file in json
    with open("detail.txt", "a") as output:
        output.write(json.dumps(c))
        output.write("\n")

    # Write a simpler overview
    with open("simple.txt", "a") as output:
        output.write("{},{},{},{},{},{},{}\n".format(
            c['id'],
            c['title'],
            c['price'],
            c['plu'],
            c['myBkOnly'],
            c['myBkOnetime'],
            c['abTesting']
        ))
